'use strict';

var dbConfig = require('../dbconfig').test;

module.exports = {

  httpPort: process.env.HTTP_PORT || 3030,
  db: {
    host: dbConfig.host,
    port: dbConfig.port,
    dbName: dbConfig.dbName,
    options: dbConfig.options
  }
};