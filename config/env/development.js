'use strict';

var dbConfig = require('../dbconfig').development;

module.exports = {

  httpPort: process.env.HTTP_PORT || 3000,
  db: {
    host: dbConfig.host,
    port: dbConfig.port,
    dbName: dbConfig.dbName,
    options: dbConfig.options
  }
};
