'use strict';

var fs = require('fs'),
  http = require('http'),
  express = require('express'),
  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  glob = require('glob'),
  config = require('./config'),
  path = require('path');

module.exports = function () {
  // Initialize express app
  var app = express();

  // Environment dependent middleware
  if (process.env.NODE_ENV === 'development') {
    // Enable logger (morgan)
    app.use(morgan('dev'));
  }

  // Request body parsing middleware should be above methodOverride
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json());
  app.use(methodOverride());

  // at first use app routes
  glob.sync('./server_app/routes/**/*.js').forEach(function (routePath) {
    require(path.resolve(routePath))(app);
  });

  app.use(function (err, req, res, next) {
    if (!err) return next();

    console.error(err.stack);

    res.status(500)
      .send('500', {error: err.stack});
  });


  http.createServer(app).listen(config.httpPort);

  return app;
};
