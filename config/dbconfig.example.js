'use strict';

module.exports = {
  "development": {
    "host": "localhost",
    "port": 27017,
    "dbName": "contacts_app",
    "options": {
      "user": "",
      "pass": "",
      "server": {
        "poolSize": process.env.DB_POOL_SIZE || 5
      }
    }
  },
  "test": {
    "host": "localhost",
    "port": 27017,
    "dbName": "contacts_app_test",
    "options": {
      "user": "",
      "pass": "",
      "server": {
        "poolSize": process.env.DB_POOL_SIZE || 5
      }
    }
  },
  "production": {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "dbName": process.env.DB_NAME,
    "options": {
      "user": process.env.DB_USERNAME,
      "pass": process.env.DB_PASSWORD,
      "server": {
        "poolSize": process.env.DB_POOL_SIZE || 5
      }
    }
  },
  "demo": {
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "dbName": process.env.DB_NAME,
    "options": {
      "user": process.env.DB_USERNAME,
      "pass": process.env.DB_PASSWORD,
      "server": {
        "poolSize": process.env.DB_POOL_SIZE || 5
      }
    }
  }
};
