/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */
'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.on('uncaughtException', err => console.error(err.stack));
console.log('Application running in', process.env.NODE_ENV, 'mode');

const init = require('./config/init')();
const config = require('./config/config');

// Init the express application and start it
var app = require('./config/express')();

console.log('Application has started on port: ', config.httpPort);

module.exports = app;
