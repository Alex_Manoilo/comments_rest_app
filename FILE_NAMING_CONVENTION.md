### Server

* controllers - contacts.controller.js
* routes - contacts.routes.js
* models - contact.model.js (register with mongoose using PascalCase + singular 'Contact') 
* custom responses - not_found.response.js