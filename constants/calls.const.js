'use strict';

const CALL_TYPES_MAP = {
  INCOMING: 'incoming',
  OUTCOMING: 'outcoming'
};

module.exports = {
  TYPES_MAP: CALL_TYPES_MAP,
};
