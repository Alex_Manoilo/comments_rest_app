'use strict';

let db = require('../../server_app/models');
let faker = require('faker');

function reformatNumber(string) {
  return string.split('-').join('');
}

function generateContact() {
  return {
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    birthDate: faker.date.past(20),
    phoneNumbers: [
      reformatNumber(faker.phone.phoneNumberFormat(0)),
      reformatNumber(faker.phone.phoneNumberFormat(0))]
  }
}

function generateCall(contact) {
  return {
    number: contact.phoneNumbers[0],
    contact: contact._id,
    startDate: faker.date.recent(3),
    duration: faker.random.number(180)
  }
}

function generateBatchOfContacts(number) {
  let results = [];

  for (let i = 0; i < number; i++) {
    results.push(generateContact());
  }

  return results;
}

function generateBatchOfCalls(contactsData) {
  let results = [];

  for (let i = 0; i < contactsData.length; i++) {
    for (let j = 0; j < 14; j++) {
      results.push(generateCall(contactsData[i]));
    }
  }

  return results;
}

function insertContactsWithCalls() {
  let contacts = generateBatchOfContacts(5);

  db.Contact.collection.insert(contacts, function (err, docs) {
    if (err) return console.error('insert contacts err: ', err);

    let calls = generateBatchOfCalls(docs.ops);
    db.Call.collection.insert(calls, function (err, docs) {
      if (err) return console.error('insert calls err: ', err);

      console.log('Contacts and calls created');
      process.nextTick(()=> {
        process.exit();
      });
    });
  });
}

insertContactsWithCalls();
