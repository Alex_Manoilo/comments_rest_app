## Prerequisites
Make sure you have installed all these prerequisites on your development machine.
* Node.js - [Download & Install Node.js](http://www.nodejs.org/download/) and the npm package manager, if you encounter any problems, you can also use this [GitHub Gist](https://gist.github.com/isaacs/579814) to install Node.js.

## Quick Install
    
```bash
$ npm install
$ cp ./config/dbconfig.example.js ./config/dbconfig.js
# update dbconfig.js with your db settings
# then check console messages and follow by printed link to setup password for the first user
```

## Running Your Application
After the install process is over, you'll be able to run your application

```bash
$ npm start
```

Your application should run on the 3000 port so in your browser just go to [http://localhost:3000](http://localhost:3000)

## Fill db with fake data
```bash
$ npm run fill_db
```

## How to install new node package
```
$ npm prune
$ npm cache clear
$ npm install package-name --save
```

## Run tests
```bash
$ NODE_ENV=test ./node_modules/mocha/bin/mocha './server_app/tests/**/*.test.js'
```
or npm run test_server