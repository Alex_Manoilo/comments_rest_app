'use strict';

const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

Schema = mongoose.Schema;

const firstNameRegexRule = [/^[0-9A-Za-z-_!@]*$/, 'First name can contain only letters, numbers, dashes, underscores, @ and !'];
const lastNameRegexRule = [/^[0-9A-Za-z-_!@]*$/, 'Last name can contain only letters, numbers, dashes, underscores, @ and !'];
const phoneRegex = /^\+?([\d]){6,14}$/;

const ContactSchema = new Schema({
  firstName: {type: String, required: true, minlength: 3, maxlength: 50, match: firstNameRegexRule},
  lastName: {type: String, required: true, minlength: 3, maxlength: 50, match: lastNameRegexRule},
  birthDate: {type: Date},
  createdAt: {type: Date, required: true, default: Date.now},
  phoneNumbers: [String]
});

ContactSchema.path('phoneNumbers').validate(function (phoneNumbers) {
  let isValid = true;
  if (phoneNumbers.length === 0) {
    return true
  }

  if (!Array.isArray(phoneNumbers)) {
    return false
  }

  phoneNumbers.forEach((number)=> {
    if (!number.match(phoneRegex)) {
      isValid = false;
    }
  });

  return isValid;
}, 'Phone numbers should be an array and can contain + and numbers only and have from 6 to 14 digits');

ContactSchema.index({"firstName": 1, "lastName": 1}, {unique: true});

ContactSchema.plugin(uniqueValidator, {
  message: "firstName, lastName pair must be unique"
});

module.exports = {
  name: 'Contact',
  schema: ContactSchema
};
