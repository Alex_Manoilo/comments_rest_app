'use strict';

const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const bluebirdPromise = require('bluebird');
const glob = require('glob');
const path = require('path');
const config = require('../../config/config.js');
const connectionUri = `${config.db.host}:${config.db.port}/${config.db.dbName}`;

const db = {};
const files = glob.sync('**/!(index).js', {cwd: path.dirname(module.filename)});

files.forEach((file) => {
  let modelData = require('./' + file);
  let modelSchema = modelData.schema;
  let modelName = modelData.name;

  modelData.schema.plugin(mongoosePaginate);
  db[modelName] = mongoose.model(modelName, modelSchema);

  // db[modelName].on('index', function(error) {
  //     console.error('index add err: ', arguments);
  // });
});

mongoose.Promise = bluebirdPromise;
mongoose.connect(connectionUri, config.db.options).then(
  () => {
  },
  err => {
    console.error("Cant connect to the db: ", err);
    process.exit();
  }
);

module.exports = db;
