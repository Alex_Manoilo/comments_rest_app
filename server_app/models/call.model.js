const mongoose = require('mongoose');
const callTypes = require('../../constants/calls.const').TYPES_MAP;
Schema = mongoose.Schema;

// we need "number" for case when appropriate contact will be removed or called number will be removed from the contact
const CallSchema = new Schema({
  number: {type: String, required: true},
  contact: {type: mongoose.Schema.Types.ObjectId, ref: 'Contact'},
  startDate: {type: Date, required: true},
  duration: {type: Number, required: true},
  type: {type: String, enum: [callTypes.INCOMING, callTypes.OUTCOMING], required: true},
  createdAt: {type: Date, required: true, default: Date.now}
});

module.exports = {
  name: 'Call',
  schema: CallSchema
};
