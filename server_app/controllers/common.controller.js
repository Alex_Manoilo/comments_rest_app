"use strict";

const errorResponse = require('./../responses/v1/server_error.response');
const _ = require('lodash');

const defaultCtrl = {
  findAll: (req, res) => {
    let paginateOptions = {
      limit: req.pagination.limit,
      offset: req.pagination.offset
    };

    let query = req.pagination.query || {};

    return req.baseModel.paginate(query, paginateOptions)
      .then((entities) => res.status(200).json(entities))
      .catch((err) => errorResponse(err, res));
  },

  findOne: function (req, res) {
    const entityId = req.params.id;

    return req.baseModel.findById(entityId)
      .then((entity) => {
        if (!entity) return Promise.reject({status: 404});

        res.status(200).json(entity);
      })
      .catch((err) => errorResponse(err, res));
  },

  create: function (req, res) {
    const entity = _.omit(req.body, ['_id']);

    return req.baseModel.create(entity)
      .then((entity) => res.status(201).json(entity))
      .catch((err) => errorResponse(err, res));
  },

  update: function (req, res) {
    const entityId = req.params.id;
    let entity = _.omit(req.body, ['_id']);
    let options = req.updateOptions || {new: true, runValidators: true, context: 'query'};

    return req.baseModel.findByIdAndUpdate(entityId, entity, options)
      .then((result) => {
        if (!result) return Promise.reject({status: 404});

        res.status(200).json(result);
      })
      .catch((err) => {
        errorResponse(err, res)
      });
  },

  destroy: function (req, res) {
    const entityId = req.params.id;

    return req.baseModel.remove({where: {id: entityId}})
      .then(() => res.status(204).json())
      .catch((err) => errorResponse(err, res));
  }
};

module.exports = defaultCtrl;
