'use strict';

const db = require('../../models');
const commonCtrl = require('../common.controller');
const _ = require('lodash');

exports.findAll = function (req, res) {
  req.baseModel = db.Contact;
  commonCtrl.findAll(req, res);
};

exports.findOne = function (req, res) {
  req.baseModel = db.Contact;
  commonCtrl.findOne(req, res);
};

exports.findCalls = function (req, res) {
  req.baseModel = db.Call;
  req.pagination.query = {
    contact: req.params.id
  };
  commonCtrl.findAll(req, res);
};

exports.create = function (req, res) {
  req.baseModel = db.Contact;
  commonCtrl.create(req, res);
};

exports.update = function (req, res) {
  // need to add hook for checking if mobile numbers were changed and if they were - we need to update appropriate calls
  // (find them by changed number and contact id and remove contact reference
  // because now that contact does not contain the called number)
  req.baseModel = db.Contact;
  commonCtrl.update(req, res);
};

exports.destroy = function (req, res) {
  req.baseModel = db.Contact;
  commonCtrl.destroy(req, res);
};
