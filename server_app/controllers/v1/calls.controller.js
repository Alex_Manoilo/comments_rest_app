'use strict';

const db = require('../../models');
const errorResponse = require('../../responses/v1/server_error.response');
const commonCtrl = require('../common.controller');

exports.findAll = function (req, res) {
  let paginateOptions = {
    limit: req.pagination.limit,
    offset: req.pagination.offset,
    populate: 'contact'
  };

  return db.Call.paginate({}, paginateOptions)
    .then((calls) => res.status(200).json(calls))
    .catch((err) => errorResponse(err, res));
};

exports.findOne = function (req, res) {
  const callId = req.params.id;

  return db.Call.findById(callId)
    .populate('contact')
    .then((call) => {
      if (!call) return Promise.reject({status: 404});

      res.status(200).json(call);
    })
    .catch((err) => errorResponse(err, res));
};

exports.create = function (req, res) {
  const contactId = req.body.contact;

  db.Contact.findById(contactId)
    .then((contact) => {
      // in real life we should have history for unsaved contacts so we can
      // save contact with flag 'saved': false and don't get them for list of contacts
      // and in the next time if contact was not found during creating call we can use number of caller
      // for searching between contacts with flag "saved": false and add found contact id to the call or create new contact and use it id
      // all these steps are required for having possibility to load history of calls from/to unsaved contacts
      if (!contact) return Promise.reject({
        status: 400,
        message: 'You can\'t save call for this contact, it does not exist'
      });

      req.baseModel = db.Call;
      commonCtrl.create(req, res);
    })
    .catch((err) => errorResponse(err, res));
};

exports.destroy = function (req, res) {
  req.baseModel = db.Call;
  commonCtrl.destroy(req, res);
};
