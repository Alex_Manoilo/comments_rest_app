'use strict';
const errorResponse = require('../responses/v1/server_error.response');

exports.retrieveParams = function (req, res, next) {
  let limit = parseInt(req.query.limit) || 10;
  let offset = parseInt(req.query.offset) || 0;

  if (limit < 0 || offset < 0) {
    let error = {
      status: 400,
      message: 'limit and offset should be positive'
    };

    return errorResponse(error, res)
  }

  req.pagination = {
    limit: limit,
    offset: offset
  };

  return next();
};
