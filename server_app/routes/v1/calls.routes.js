'use strict';

const express = require('express');
const callsCtrl = require('../../controllers/v1/calls.controller');
const pagingMiddleware = require('../../middlewares/paging-params.mdlw');

module.exports = function (app) {
  let router = express.Router();

  router.get('/', pagingMiddleware.retrieveParams, callsCtrl.findAll);
  router.post('/', callsCtrl.create);

  router.get('/:id', callsCtrl.findOne);
  router.delete('/:id', callsCtrl.destroy);

  app.use('/api/v1/calls', router);
};
