'use strict';

const express = require('express');
const contactsCtrl = require('../../controllers/v1/contacts.controller');
const pagingMiddleware = require('../../middlewares/paging-params.mdlw');

module.exports = function (app) {
  let router = express.Router();

  router.get('/', pagingMiddleware.retrieveParams, contactsCtrl.findAll);
  router.post('/', contactsCtrl.create);

  router.get('/:id', contactsCtrl.findOne);
  router.get('/:id/calls', pagingMiddleware.retrieveParams, contactsCtrl.findCalls);
  router.put('/:id', contactsCtrl.update);
  router.delete('/:id', contactsCtrl.destroy);

  app.use('/api/v1/contacts', router);
};
